#include "MASS.h"
#include "TestPlace.h"
#include "TestAgent.h"
#include "Timer.h"

using namespace std;

int main(int argc, char *argv[]) {
    if (argc != 10) {
        cerr << "Usage: ./test username password machinefile port nProc numThreads #testAgents simSize [show]" << endl;
        return -1;
    }

    char *arguments[4];
    arguments[0] = argv[1]; // username
    arguments[1] = argv[2]; // password
    arguments[2] = argv[3]; // machinefile
    arguments[3] = argv[4]; // port
    int nProc = atoi(argv[5]);
    int numThreads = atoi(argv[6]);
    int nAgents = atoi(argv[7]);    // # testAgents
    int simSize = atoi(argv[8]);     // simulation space
    bool show = (argv[9][0] == 's');


    Timer timer;

    MASS::init(arguments, nProc, numThreads);

    // initialize testPlaces
    char *msg = (char *) ("genericcreation\0");

    std::cout << "Creating {" << simSize * simSize << "} TestPlaces now..." << std::endl;

    Places *testPlaces = new Places(1, "TestPlace", 1, msg, 16, 2, simSize, simSize);

    std::cout << "Finished creating TestPlaces!" << std::endl;

    void *showPtr = &show;
    testPlaces->callAll(TestPlace::init_, showPtr, sizeof(bool));

    std::cout << "Creating {" << nAgents << "} TestAgents now..." << std::endl;

    // initialize testAgents

    Agents *testAgents = new Agents(2, "TestAgent", msg, 16, testPlaces, nAgents);

    std::cout << "Done creating {" << nAgents << "} TestAgents now..." << std::endl;

    testAgents->callAll(TestAgent::init_, showPtr, sizeof(bool));

    testAgents->callAll(TestAgent::randomHop_);

    testAgents->callAll(TestAgent::printState_, showPtr, sizeof(bool));

    // check to see if new agents are correctly reflected on places
    testPlaces->callAll(TestPlace::testAgentsOnPlace_, showPtr, sizeof(bool));

 
    // Testing neighbor functionality ------------------------------------------------------------------------------

    // testing adding neighbors - random, Moore, and VonNeumann - and exchanging information

    std::cout << "TestPlace's neighbors before any alterations... " << std::endl;
    testPlaces->callAll(TestPlace::printPlaceNeighbors_, showPtr, sizeof(bool));

    std::cout << "Now adding a random neighbor to each TestPlace ... " << std::endl;
    testPlaces->callAll(TestPlace::addRandomPlaceNeighbor_);
    testPlaces->callAll(TestPlace::printPlaceNeighbors_, showPtr, sizeof(bool));

    std::cout << "Now clearing and adding Moore neighbors to each TestPlace... " << std::endl;
    testPlaces->callAll(TestPlace::clearAndAddMoore_);
    testPlaces->callAll(TestPlace::printPlaceNeighbors_, showPtr, sizeof(bool));

    std::cout << "Now clearing and adding VonNeumann neighbors to each TestPlace... " << std::endl;
    testPlaces->callAll(TestPlace::clearAndAddVNeumann_);
    testPlaces->callAll(TestPlace::printPlaceNeighbors_, showPtr, sizeof(bool));

/*

    // testing exchange all - exchanging messages with current neighbors (VonNeumann)
    std::cout << "Now testing exchanging a string message with current VonNeumann neighbors... " << std::endl;
    testPlaces->exchangeAll(1,TestPlace::exchangeMyMessage_);
    testPlaces->callAll(TestPlace::printMyInMessages_, showPtr, sizeof(bool));

    // End neighbor testing --------------------------------------------------------------------------------------------
    std::cout << "Place neighbor functionality testing complete!" << std::endl;

    */

    return 0;
}
