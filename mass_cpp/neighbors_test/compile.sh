#!/bin/sh
export MASS_DIR=../mass_cpp_core
g++ -Wall TestPlace.cpp -I$MASS_DIR/source -shared -fPIC -std=c++11 -o TestPlace
g++ -Wall TestAgent.cpp -I$MASS_DIR/source -shared -fPIC -std=c++11 -o TestAgent
g++ -std=c++11 -Wall main.cpp Timer.cpp -I$MASS_DIR/source -L$MASS_DIR/ubuntu -lmass -I$MASS_DIR/ubuntu/ssh2/include -L$MASS_DIR/ubuntu/ssh2/lib -lssh2 -o test
